import argparse
from NeuralNetwork import NeuralNetwork as NN

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to input dataset of images")
ap.add_argument("-m", "--model", required=True,
                help="path to output trained model")
args = vars(ap.parse_args())


nn = NN(args['dataset'])
nn.traintModel()
nn.saveModel(args['model'])
